#!/bin/bash

apt-get update && \
apt-get install -y build-essential cmake git pkg-config libgtk-3-dev \
    libavcodec-dev libavformat-dev libswscale-dev libv4l-dev \
    libxvidcore-dev libx264-dev libjpeg-dev libpng-dev libtiff-dev \
    gfortran openexr libatlas-base-dev python3-dev python3-numpy \
    libtbb2 libtbb-dev libdc1394-22-dev libopenblas-dev liblapacke-dev \
    libopenblas-base liblapacke libeigen3-dev libjpeg8-dev libjpeg-turbo8-dev \
    libtiff5-dev libwebp-dev libopenjp2-7-dev libgdal-dev vim && \
python3 -m pip install --user numpy

