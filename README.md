# how to set up opencv dev environment

1. clone the repo:
    ```
    git clone https://gitlab.com/cv-algos/opencv-dev && cd opencv-dev
2. run docker build bash script:
    ```
    ./build_docker.sh
3. start the docker
    ```
    ./run_docker.sh
3. clone opencv:
    ```
    git clone --recursive https://github.com/opencv/opencv-python.git
4. copy the build.sh bash script into the opencv subdirectory
    ```
    cp ./build.sh ./opencv-python/opencv
5. build opencv
    ```
    cd ./opencv-python/opencv && mkdir build && ./build.sh
6. build opencv-python
    ```
    cd .. && pip wheel . --verbose
7. install the wheel
    ```
    pip install --force-reinstall opencv_python-4.8.0.76-cp38-cp38-linux_x86_64.whl
8. check if sfm was installed
    ```
    root@dev:/user/home# python3 -c "import cv2; cv2.sfm_BundleAdjuster.create()"
9. note that sfm wasn't built :(
```
Traceback (most recent call last):
  File "<string>", line 1, in <module>
AttributeError: module 'cv2' has no attribute 'sfm_BundleAdjuster'

