#!/bin/bash

docker run --privileged=true \
           -u 0 \
           --name $USER-opencv-dev \
           -v /disk01/users/seth:/disk01/users/seth:Z \
           --network=host --rm -it \
           opencv-devel:latest bash
