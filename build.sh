#!/bin/bash
export OPENCV_ENABLE_NONFREE:BOOL=ON
cd build && \
cmake .. -D CMAKE_BUILD_TYPE=RELEASE \
      -D OPENCV_ENABLE_NONFREE=ON \
      -D OPENCV_PYTHON3_INSTALL_PATH=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
      -D PYTHON_EXECUTABLE=$(which python3) \
      -D CMAKE_INSTALL_PREFIX=/usr/local \
      -D INSTALL_C_EXAMPLES=ON \
      -D INSTALL_PYTHON_EXAMPLES=ON \
      -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
      -D PYTHON_EXECUTABLE=/usr/bin/python3 \
      -D BUILD_EXAMPLES=ON \
      -D SFM_DEPS_OK=ON && \
make clean && make -j4
