ARG version
FROM ubuntu-20.04
SHELL ["/bin/bash", "-c"]

# Set some environment variables
ENV DEBIAN_FRONTEND=noninteractive LANG=C.UTF-8

# Setup Environment:
ARG DEBIAN_FRONTEND=noninteractive

# Install Common Development Tools completed in base image:
RUN  apt-get -q clean \
    &&  apt-get -q -y update \
    &&  apt-get -q -y upgrade \
    &&  apt-get -q -y install \
        build-essential \
        curl \
        unzip \
        swig \
        python3.8 \
        python3-pip \
        git \
        ffmpeg \
        nano \
        vim \
        python3-dev \
        openssl \
        tar \
        wget \
        cmake \
        pkg-config \
        libgtk-3-dev \
        libavcodec-dev \
        libavformat-dev \
        libswscale-dev \
        libv4l-dev \
        libxvidcore-dev \
        libx264-dev \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        gfortran \
        openexr \
        libatlas-base-dev \
        python3-numpy \
        libtbb2 \
        libtbb-dev \
        libdc1394-22-dev \
        libopenblas-dev \
        liblapacke-dev \
        libopenblas-base \
        liblapacke \
        libeigen3-dev \
        libjpeg8-dev \
        libjpeg-turbo8-dev \
        libtiff5-dev \
        libwebp-dev \
        libopenjp2-7-dev \
        libgdal-dev \
        python3-opencv \
        libopencv-dev \
        openssh-client \
        sudo \
        libboost-system-dev \ 
        libboost-thread-dev \
        libboost-program-options-dev \
        libboost-test-dev \
        libgl1-mesa-glx \
        libglib2.0-0 \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libavcodec-dev \
        libavformat-dev \
        libswscale-dev \
        libavresample-dev \
        libgstreamer1.0-dev \
        libgstreamer-plugins-base1.0-dev \
        libxvidcore-dev \
        x264 \
        libx264-dev \
        libfaac-dev \
        libmp3lame-dev \
        libtheora-dev \
        libfaac-dev \
        libmp3lame-dev \
        libvorbis-dev \
        libtbb-dev \
        libatlas-base-dev \
        gfortran \
        libgflags2.2 \
        libgflags-dev \
        libgoogle-glog-dev \
    && apt-get -q clean


# Install Python3 dependencies
RUN pip3 install --upgrade pip && \
    pip3 install \
        wheel \
        setuptools \
        cmake \
        six \
        python-dateutil \
        pytz \
        scipy \
        numpy \
        pandas \
        fonttools \
        cycler \
        kiwisolver \
        pillow \
        pyparsing \
        contourpy \
        packaging \
        matplotlib \
        urllib3 \
        idna \
        certifi \
        charset-normalizer \
        requests \
        patch-ng \
        colorama \
        pyJWT \
        MarkupSafe \
        Jinja2 \
        tqdm \
        pluginbase \
        fasteners \
        bottle \
        PyYAML \
        node-semver \
        distro \
        pygments \
        parse \
        conan \
        tifffile \
        h5py \
        numexpr \
        tables \
        opencv-contrib-python
